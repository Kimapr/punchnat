extern crate stunclient;
extern crate udt;
extern crate rand;

mod punchnat;

use std::net::{SocketAddr,ToSocketAddrs};

fn get_targets(args: &mut std::env::Args) -> Vec<(punchnat::TunnelType,u16,SocketAddr)> {
    let mut v=vec!();
    loop {
        let a1=args.next();
        match a1 {
            Some(a1) => {
                let (a2,a3)=(args.next().expect("invalid number of arguments"),args.next().expect("invalid number of arguments"));
                let ttype=match a1.as_str() {
                    "stream" => punchnat::TunnelType::Stream,
                    "datagram" => punchnat::TunnelType::Datagram,
                    _ => {panic!("invalid socket type");}
                };
                let vport=a2.parse().expect("invalid vport");
                let addr=a3.to_socket_addrs().expect("invalid addr").next().unwrap();
                v.push((ttype,vport,addr));
            },
            None => {break;}
        }
    }
    v
}

fn main() {
    udt::init();
    let usage="Usage:
  punchnat -help
    # print this shit

  punchnat -coord <addr>
    # coordinate NAT traversals, bind at addr

  punchnat -server <coordinator addr> <service name> <<id> <target addr>> [...]
    # connect to coordinator coordinator addr, register self as service name, wait for clients

  punchnat -client <coordinator addr> <service name> <<id> <bind addr>> [...]
    # connect to coordinator addr, build a NAT traversed UDP tunnel to service name";
    let mut args=std::env::args();
    args.next();
    let s_mode=args.next();
    println!("Welcome to punchnat. Run with -help for usage.");
    match s_mode {
        Some(mode) => match mode.as_str() {
            "-coord" => {
                println!("Coordinator server running...");
                let addr=args.next().expect("FAIL: Bind address expected");
                punchnat::coordinator::run(addr);
            },
            "-server" => {
                println!("Running a server behind NAT...");
                let coord=args.next().expect("FAIL: Coordinator address expected").to_socket_addrs().expect("Invalid addr").next().unwrap();
                let name=args.next().expect("FAIL: Service name expected");
                let server=punchnat::server::run(coord,&name,get_targets(&mut args));
            },
            "-client" => {
                println!("Connecting to a server behind NAT...");
                let coord=args.next().expect("FAIL: Coordinator address expected").to_socket_addrs().expect("Invalid addr").next().unwrap();
                let name=args.next().expect("FAIL: Service name expected");
                let client=punchnat::client::run(coord,&name,get_targets(&mut args));
            },
            "-help" => {
                println!("{}",usage);
            },
            _ => {
                panic!("Invalid mode");
            }
        },
        None => {
            //TODO: gui
            println!("There will be a GUI here, sometime");
            panic!("GUI not implemented");
        }
    }
}
