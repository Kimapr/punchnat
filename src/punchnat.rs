use std::net::{SocketAddr, UdpSocket, ToSocketAddrs};
use stunclient::StunClient;

#[derive(Copy, Clone)]
pub enum NatType {
    Symmetric(SocketAddr),
    Cone(SocketAddr),
}

#[derive(Copy, Clone)]
pub enum PunchError {
    NatTypeDiscoveryError(TestNatError),
    ConnectError,
    HolepunchError,
}

#[derive(Debug, Copy, Clone)]
pub enum TestNatError {
    StunBindError,
}

use NatType::*;
use PunchError::*;
use TestNatError::*;

fn test_nat(sock: &UdpSocket) -> Result<NatType,TestNatError> {
    let mut servers=Vec::<SocketAddr>::new();
    servers.push("stun.l.google.com:19302".to_socket_addrs().unwrap().next().unwrap());
    servers.push("stun1.l.google.com:19302".to_socket_addrs().unwrap().next().unwrap());
    let mut port: u16 = 0;
    let mut addrl = "0.0.0.0:0".parse().unwrap();
    for serv in servers.iter() {
        let stuncl=StunClient::new(*serv);
        let res=stuncl.query_external_address(sock);
        match res {
            Ok(addr) => {
                addrl=addr;
                match port {
                    0 => {
                        port=addr.port();
                    },
                    _ => {
                        let p2=addr.port();
                        if p2 != port {
                            return Ok(Symmetric(SocketAddr::new(addrl.ip(),0)));
                        }
                    }
                }
            },
            Err(perror) => {
                return Err(StunBindError);
            }
        }
    }
    Ok(Cone(addrl))
}

mod pn_proto {
    pub const PN_Magic: u8 = 199; // [ magic: u64 ]
    pub const PT_Connect: u8 = 130; // [ port_number: u16 ]

    pub const CN_Announce: u8 = 72; // [ name_size: u8 | name: str ]
    pub const CN_Reject: u8 = 28;
    pub const CN_Accept: u8 = 105;
    pub const CN_Connect: u8 = 45; // [ name_size: u8 | name: str ]

    pub const CT_Start: u8 = 57; // [ ntype: u8 | ip: u32 | [port: u16] ]
    pub const CT_Magic: u8 = 125; // [ magic: u64 ]
    pub const CT_Nat_Bad: u8 = 0;
    pub const CT_Nat_Good: u8 = 1;
}

pub mod coordinator {
    use std::net::{UdpSocket, SocketAddr, ToSocketAddrs, TcpStream, TcpListener};
    use std::thread;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use udt::*;
    use std::io::prelude::*;
    use std::io::Cursor;
    use std::sync::{Arc, Mutex};
    use std::sync::mpsc::{channel, Receiver, Sender};
    use std::collections::HashMap;
    use std::time::Duration;
    use super::pn_proto::*;
    
    struct PServer {
        addr: SocketAddr,
    }

    fn parse_name(client: UdtSocket) -> Option<(UdtSocket,u8,String)> {
        let mut buff = [0; 512];
        let size=client.recvmsg(&mut buff);
        match size {
            Ok(size) => {
                let size=size as usize;
                if (size>1) {
                    let mut cur=Cursor::new(&buff[0..size]);
                    let mtype=cur.read_u8().unwrap();
                    let namesize = cur.read_u8().unwrap() as usize;
                    let mut namebuf: Vec<u8> = Vec::with_capacity(namesize);
                    namebuf.resize(namesize,0);
                    let nmsize=cur.read(&mut namebuf);
                    match nmsize {
                        Ok(nmsize) => {
                            if (nmsize==namesize) {
                                let name=String::from_utf8(namebuf);
                                match name {
                                    Ok(name) => {
                                        return Some((client,mtype,name));
                                    },
                                    Err(_) => {
                                        client.close().unwrap();
                                    },
                                }
                            } else {
                                client.close().unwrap();
                            }
                        },
                        Err(_) => {
                            client.close().unwrap();
                        },
                    }
                } else {
                   client.close().unwrap(); 
                }
            },
            Err(_) => {
                client.close().unwrap();
            }
        }
        None
    }

    pub fn run<T: ToSocketAddrs>(baddr: T) {
        let mut listener=UdtSocket::new(SocketFamily::AFInet,SocketType::Datagram).unwrap();
        let laddr=baddr.to_socket_addrs().expect("Failed to parse addr").next().unwrap();
        listener.bind(laddr);
        listener.listen(64);
        let pservers = Arc::new(Mutex::new(HashMap::<String,PServer>::new()));
        while true {
            let (mut client,caddr)=listener.accept().expect("Failed to receive connection");
            println!("Client!");
            let pservers=Arc::clone(&pservers);
            thread::spawn(move || {
                let (mut client,mtype,name)=parse_name(client).unwrap();
                match mtype {
                    CN_Announce => {
                        let pserver=PServer{
                            addr: caddr,
                        };
                        let mut pserversl=pservers.lock().unwrap();
                        if pserversl.contains_key(&name) {
                            client.sendmsg(&[CN_Reject]);
                        } else {
                            client.sendmsg(&[CN_Accept]);
                        }
                        pserversl.insert(name.clone(),pserver);
                        drop(pserversl);
                        println!("It's a server!");
                        loop {
                            match client.getstate() {
                                UdtStatus::CONNECTED => {
                                    thread::sleep(Duration::from_millis(50));
                                }
                                _ => {
                                    println!("Removing server from list");
                                    client.close().unwrap();
                                    let mut pserversl = pservers.lock().unwrap();
                                    pserversl.remove(&name);
                                    return;
                                }
                            }
                        }
                    },
                    CN_Connect => {
                        let pserversl=pservers.lock().unwrap();
                        if !pserversl.contains_key(&name) {
                            println!("No server found for client");
                            client.sendmsg(&[CN_Reject]);
                            drop(pserversl);
                            client.close();
                            return;
                        }
                        let saddr=pserversl.get(&name).unwrap().addr;
                        drop(pserversl);
                        let sock=UdtSocket::new(SocketFamily::AFInet,SocketType::Datagram).unwrap();
                        sock.bind(laddr).unwrap();
                        sock.connect(saddr).expect("Connection failed");
                        client.sendmsg(&[CN_Accept]);
                        println!("Proxying datagrams...");
                        thread::spawn(move || {
                            let mut epoll=Epoll::create().unwrap();
                            let mut flags=EpollEvents::empty();
                            flags.insert(UDT_EPOLL_IN);
                            flags.insert(UDT_EPOLL_ERR);
                            epoll.add_usock(&client,Some(flags));
                            epoll.add_usock(&sock,Some(flags));
                            let mut buf = [0; 512];
                            loop {
                                let (ins,errs)=epoll.wait(-1,true).unwrap();
                                for usock in errs.iter() {
                                    println!("Error!");
                                    let err1=sock.close();
                                    let err2=client.close();
                                    err1.unwrap();
                                    err2.unwrap();
                                    return;
                                }
                                for usock in ins.iter() {
                                    println!("Event from");
                                    let size = usock.recvmsg(&mut buf).unwrap() as usize;
                                    match usock {
                                        usock if *usock == client => {
                                            println!("Packet from client to server");
                                            sock.sendmsg(&buf[0..size]);
                                        },
                                        usock if *usock == sock => {
                                            println!("Packet from server to client");
                                            client.sendmsg(&buf[0..size]);
                                        },
                                        _ => {
                                            panic!("wtf");
                                        }
                                    }
                                }
                            }
                        });
                    },
                    _ => {
                        client.close().unwrap();
                    },
                }
            });
        }
    }
}

fn nat_msg(ntype: NatType) -> Vec<u8> {
    use std::io::Cursor;
    use std::io::prelude::*;
    use pn_proto::*;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use std::net::IpAddr;

    let mut msgb = Cursor::new(Vec::new());
    msgb.write(&[CT_Start]);
    match ntype {
        Symmetric(addr) => {
            msgb.write(&[CT_Nat_Bad]);
            match addr.ip() {
                IpAddr::V4(ip) => {
                    msgb.write(&ip.octets());
                },
                _ => {
                    panic!("only ipv4 is supported");
                }
            }
        },
        Cone(addr) => {
            msgb.write(&[CT_Nat_Good]);
            match addr.ip() {
                IpAddr::V4(ip) => {
                    msgb.write(&ip.octets());
                },
                _ => {
                    panic!("only ipv4 is supported");
                }
            }
            msgb.write_u16::<NetworkEndian>(addr.port());
        }
    }
    msgb.into_inner()
}

fn punchnat(sock: &UdpSocket, ntype: NatType, magic: u64) {
    println!("Punching NAT");
    use std::time::{Instant, Duration};
    use std::thread;
    use std::io::Cursor;
    use std::io::prelude::*;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use std::net::SocketAddr;

    let start=Instant::now();
    let end=start+Duration::from_secs(30);
    let mut num: u64 = 0;
    let mut magicbuf=Cursor::new(Vec::new());
    magicbuf.write_u64::<NetworkEndian>(magic);
    let magic=magicbuf.into_inner();
    let timeout=sock.read_timeout().unwrap();
    sock.set_read_timeout(Some(Duration::from_millis(1))).unwrap();
    let mut buf = [0;8];
    match ntype {
        Symmetric(addr) => {
            'outer: loop {
                let ip=addr.ip();
                for port in 1..=65535 {
                    sock.send_to(magic.as_slice(),SocketAddr::new(ip,port));
                    num=num+1;
                    if num>1000 {
                        let res=sock.recv_from(&mut buf);
                        match res {
                            Ok((size,saddr)) => {
                                println!("mesag!");
                                if size==8 && magic.as_slice()==&buf {
                                    sock.connect(saddr);
                                    println!("pwn!");
                                    break 'outer;
                                }
                            },
                            Err(_) => {}
                        }
                        end.checked_duration_since(Instant::now()).expect("Timed Out");
                        num=num-1000;
                    }
                }
            }
        },
        Cone(addr) => {
            sock.connect(addr);
            loop {
                sock.send(magic.as_slice());
                num=num+1;
                if num>1000 {
                    let res=sock.recv(&mut buf);
                    match res {
                        Ok(size) => {
                            println!("mesag!");
                            if size==8 && magic.as_slice()==&buf {
                                println!("pwn!");
                                break;
                            }
                        },
                        Err(_) => {}
                    }
                    end.checked_duration_since(Instant::now()).expect("Timed Out");
                    num=num-1000;
                }
            }
        }
    }
    println!("NAT traversal successful!");
    let ss=Instant::now()+Duration::from_millis(1000);
    loop {
        sock.recv(&mut buf);
        match ss.checked_duration_since(Instant::now()) {
            Some(_) => {},
            None => {
                return;
            }
        }
    }
}

#[derive(Copy,Clone)]
pub enum TunnelType {
    Stream,
    Datagram
}

use udt::*;
use std::net::TcpStream;

fn tunnel_stream(mut client: TcpStream, sock: UdtSocket) {
    use std::thread;
    use std::io::prelude::*;
    use std::net::Shutdown;
    use std::time::Duration;
    
    let (sock2,mut client2)=(sock.clone(),client.try_clone().unwrap());
    thread::spawn(move || {
        let mut buf=[0;1420];
        loop {
            let size=client.read(&mut buf);
            match match size {
                Ok(size) => {
                    if size>0 {
                        Some(size)
                    } else {
                        thread::sleep(Duration::from_millis(100));
                        None
                    }
                },
                Err(a) => {
                    sock2.close();
                    client.shutdown(Shutdown::Both);
                    println!("Error: {:?}",a);
                    panic!("connectivity error, local side closed");
                    None
                }
            } {
                Some(size) => {
                    //println!("yes msg from tcp {} bytes",size);
                    let rsize=sock2.send(&buf[0..size]).unwrap();
                    //println!("sent out {} bytes",rsize);
                },
                None => {}
            }
        }
    });
    let mut buf=[0;1420];
    loop {
        let ll=buf.len();
        let size=sock.recv(&mut buf,ll);
        match size {
            Ok(size) => {
                let size=size as usize;
                //println!("yes msg from udt {} bytes",size);
                client2.write(&buf[0..size]).unwrap();
            },
            Err(_) => {
                client2.shutdown(Shutdown::Both);
                sock.close();
                panic!("connectivity error, remote side closed");
            }
        }
    }
}

fn tunnel_client(sock: &UdpSocket, raddrs: &[(TunnelType,u16,SocketAddr)]) {
    use udt::*;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use std::io::Cursor;
    use std::io::prelude::*;
    use std::thread;
    use std::net::{TcpListener, TcpStream, UdpSocket, Shutdown};
    use pn_proto::*;

    let (laddr,raddr)=(sock.local_addr().unwrap(),sock.peer_addr().unwrap());
    let usock=UdtSocket::new(SocketFamily::AFInet, SocketType::Stream).unwrap();
    usock.bind_from(sock.try_clone().unwrap()).unwrap();
    usock.connect(raddr).unwrap();
    let raddrs2=raddrs.to_vec();
    let mut threads=vec!();
    for i in raddrs2.iter() {
        let (ttype,vport,addr)=*i;
        threads.push(thread::spawn(move || {
            println!("Attempting to tunnel to vport {} on {}",vport,addr);
            let mut bufr: [u8;2] = [0;2];
            let mut buf = Cursor::new(&mut bufr[..]);
            buf.write_u16::<NetworkEndian>(vport);
            match ttype {
                Stream => {
                    println!("Listening for stream connections to vport {} on {}",vport,addr);
                    let serv=TcpListener::bind(addr).expect("Failed to bind socket");
                    for client in serv.incoming() {
                        let mut bufr=bufr.clone();
                        let client = client.unwrap();
                        println!("Incoming tunnel connection on stream vport {}",vport);
                        thread::spawn(move || {
                            let sock=UdtSocket::new(SocketFamily::AFInet, SocketType::Stream).unwrap();
                            sock.bind(laddr).unwrap();
                            sock.connect(raddr).unwrap();
                            sock.send(&bufr[..]);
                            sock.recv(&mut bufr[..0],1).unwrap();
                            match bufr[0] {
                                CN_Accept => {
                                    println!("Tunnel connection accepted on stream vport {}",vport);
                                    tunnel_stream(client,sock);
                                },
                                CN_Reject => {
                                    println!("Tunnel connection rejected on stream vport {}",vport);
                                    sock.close();
                                    client.shutdown(Shutdown::Both);
                                },
                                _ => {
                                    sock.close();
                                    client.shutdown(Shutdown::Both);
                                    panic!("Broken peer");
                                }
                            }
                        });
                    }
                },
                Datagram => {
                    unimplemented!();
                }
            }
        }));
    }
    for i in threads.drain(..) {
        i.join();
    }
}

fn tunnel_server(sock: &UdpSocket, raddrs: &[(TunnelType,u16,SocketAddr)]) {
    use udt::*;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use std::io::Cursor;
    use std::io::prelude::*;
    use std::thread;
    use std::net::{TcpListener, TcpStream, UdpSocket};
    use pn_proto::*;

    let (laddr,raddr)=(sock.local_addr().unwrap(),sock.peer_addr().unwrap());
    //let dsock=UdtSocket::new(SocketFamily::AFInet, SocketType::Datagram).unwrap();
    //dsock.bind_from(sock.try_clone().unwrap());
    //dsock.listen(64).unwrap();
    let ssock=UdtSocket::new(SocketFamily::AFInet, SocketType::Stream).unwrap();
    ssock.bind_from(sock.try_clone().unwrap());
    //ssock.bind(laddr).unwrap();
    ssock.listen(64).unwrap();
    let raddrs2=raddrs.to_vec();
    thread::spawn(move || {
        loop {
            let (client,_)=ssock.accept().unwrap();
            let raddrs2=raddrs2.clone();
            thread::spawn(move || {
                let mut buf=[0;2];
                client.recv(&mut buf,2);
                let mut cur=Cursor::new(&buf);
                let mut addr=None;
                let port=cur.read_u16::<NetworkEndian>().unwrap();
                for i in raddrs2.iter() {
                    let (ttype,vport,aaddr)=*i;
                    if vport==port {
                        if let TunnelType::Stream = ttype {
                            addr=Some(aaddr);
                            break;
                        }
                    }
                }
                match addr {
                    Some(addr) => {
                        let clientc=TcpStream::connect(addr);
                        match clientc {
                            Ok(clientc) => {
                                client.send(&[CN_Accept]);
                                tunnel_stream(clientc,client);
                            },
                            Err(_) => {
                                client.send(&[CN_Reject]);
                            }
                        }
                    },
                    None => {
                        client.send(&[CN_Reject]);
                    }
                }
            });
        }
    }).join();
    //loop {
    //    let (client,_)=dsock.accept().unwrap();
    //    client.close(); // lol
    //}
}

fn read_nat<T: std::io::Read>(bufb: &mut T) -> NatType {
    use std::net::{SocketAddr, IpAddr, Ipv4Addr};
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use pn_proto::*;
    
    let ntypeb = bufb.read_u8().unwrap();
    let mut ipoc: [u8;4] = [0;4];
    bufb.read(&mut ipoc).unwrap();
    println!("ntypeb: {}",ntypeb);
    match ntypeb {
        CT_Nat_Bad => {
            let addr=SocketAddr::new(IpAddr::V4(Ipv4Addr::from(ipoc)),0);
            println!("[BAD] Remote NAT: Symmetric; addr: {}",addr.ip());
            return Symmetric(addr);
        },
        CT_Nat_Good => {
            let addr=SocketAddr::new(IpAddr::V4(Ipv4Addr::from(ipoc)),bufb.read_u16::<NetworkEndian>().unwrap());
            println!("[GOOD] Remote NAT: Cone; addr: {}",addr);
            return Cone(addr);
        },
        _ => {
            panic!("Broken peer");
        }
    }
}

pub mod client {
    use std::net::{UdpSocket, SocketAddr, Ipv4Addr, IpAddr};
    use super::NatType;
    use super::NatType::*;
    use super::{test_nat, punchnat, tunnel_client, nat_msg, read_nat};
    use super::pn_proto::*;
    use udt::*;
    use std::io::prelude::*;
    use std::io::Cursor;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use super::TunnelType;
    
    pub fn run(addr: SocketAddr, name: &str, raddrs: Vec<(TunnelType,u16,SocketAddr)>) {
        assert!(name.len()<256,"Service name too big");
        println!("Testing your NAT...");
        let sock=UdpSocket::bind("0.0.0.0:0").expect("Failed to bind socket");
        let ntype=test_nat(&sock).expect("Failed to test NAT");
        match ntype {
            Symmetric(_) => {
                println!("[BAD] You have symmetric NAT");
            },
            Cone(_) => {
                println!("[GOOD] You have cone NAT");
            }
        }
        println!("Connecting...");
        let csock=UdtSocket::new(SocketFamily::AFInet,SocketType::Datagram).unwrap();
        csock.bind("0.0.0.0:0".parse().unwrap());
        csock.connect(addr);
        let mut name_b = name.as_bytes().to_vec();
        let mut msg: Vec<u8> = vec!(CN_Connect,name.len() as u8);
        msg.append(&mut name_b);
        csock.sendmsg(msg.as_slice());
        let mut buf = [0; 512];
        let size=csock.recvmsg(&mut buf).unwrap();
        assert!(size==1,"Broken coordinator");
        match buf[0] {
            CN_Accept => {
                csock.sendmsg(nat_msg(ntype).as_slice()).unwrap();
                let mut buf = [0; 512];
                let mut magic: u64 = 0;
                let mut antype: NatType = Symmetric("0.0.0.0:0".parse().unwrap());
                loop {
                    let size = csock.recvmsg(&mut buf).unwrap();
                    println!("Message from server!");
                    let mut bufb = Cursor::new(buf);
                    match bufb.read_u8().unwrap() {
                        CT_Start => {
                            antype=read_nat(&mut bufb);
                        },
                        CT_Magic => {
                            magic = bufb.read_u64::<NetworkEndian>().unwrap();
                            println!("Magic number: {}",magic);
                            break;
                        },
                        _ => {
                            panic!("Broken server");
                        }
                    }
                    buf.fill(0);
                }
                csock.close().unwrap();
                punchnat(&sock,antype,magic);
                tunnel_client(&sock,raddrs.as_slice());
            },
            CN_Reject => {
                panic!("Coordinator rejected connection");
            },
            _ => {
                panic!("Broken coordinator");
            }
        }
    }
}

pub mod server {
    use std::net::{UdpSocket, SocketAddr};
    use udt::*;
    use std::io::Cursor;
    use std::io::prelude::*;
    use super::NatType;
    use super::NatType::*;
    use super::{test_nat, punchnat, tunnel_server, nat_msg, read_nat};
    use super::pn_proto::*;
    use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
    use std::net::IpAddr;
    use rand::prelude::*;
    use std::thread;
    use super::TunnelType;
    use std::time::Duration;
    
    fn gen_magic() -> (Vec<u8>,u64) {
        let mut msgb = Cursor::new(Vec::new());
        msgb.write(&[CT_Magic]);
        let num=rand::random();
        msgb.write_u64::<NetworkEndian>(num);
        (msgb.into_inner(),num)
    }
    
    pub fn run(addr: SocketAddr, name: &str, raddrs: Vec<(TunnelType,u16,SocketAddr)>) {
        assert!(name.len()<256,"Service name too big");
        let ssock=UdtSocket::new(SocketFamily::AFInet,SocketType::Datagram).unwrap();
        ssock.bind("0.0.0.0:0".parse().unwrap()).unwrap();
        let laddr=ssock.getsockname().unwrap();
        ssock.listen(64).unwrap();
        let namestr=name.to_string();
        thread::spawn(move || {
            let name=namestr.as_str();
            loop {
                let csock=UdtSocket::new(SocketFamily::AFInet,SocketType::Datagram).unwrap();
                csock.bind(laddr).unwrap();
                let mut name_b = name.as_bytes().to_vec();
                let mut msg: Vec<u8> = vec!(CN_Announce,name.len() as u8);
                msg.append(&mut name_b);
                csock.connect(addr).unwrap();
                csock.sendmsg(msg.as_slice()).unwrap();
                let mut buf=[0;512];
                let size=csock.recvmsg(&mut buf).unwrap();
                match buf[0] {
                    CN_Accept => {
                        println!("Registration successful");
                        loop {
                            match csock.getstate() {
                                UdtStatus::CONNECTED => {
                                    thread::sleep(Duration::from_millis(50));
                                }
                                _ => {
                                    println!("Disconnected");
                                    break;
                                }
                            }
                        }
                    },
                    CN_Reject => {
                        println!("Registration rejected");
                        thread::sleep(Duration::from_millis(1000));
                    },
                    _ => {
                        println!("Broken coordinator");
                        thread::sleep(Duration::from_millis(1000));
                    }
                }
                csock.close();
            }
        });
        println!("Listening for clients...");
        loop {
            let (client,caddr) = ssock.accept().unwrap();
            println!("Client!");
            let raddrs=raddrs.clone();
            thread::spawn(move || {
                if caddr==addr {
                    let sock=UdpSocket::bind("0.0.0.0:0").unwrap();
                    let ntype=test_nat(&sock).unwrap();
                    match ntype {
                        Symmetric(_) => {
                            println!("[BAD] You have symmetric NAT");
                        },
                        Cone(_) => {
                            println!("[GOOD] You have cone NAT");
                        }
                    }
                    client.sendmsg(nat_msg(ntype).as_slice()).unwrap();
                    let (mmsg,magic)=gen_magic();
                    println!("Magic number: {}",magic);
                    client.sendmsg(mmsg.as_slice()).unwrap();
                    let mut buf=[0;512];
                    let size=client.recvmsg(&mut buf);
                    let mut bufb=Cursor::new(buf);
                    let mut antype;
                    match bufb.read_u8().unwrap() {
                        CT_Start => {
                            antype=read_nat(&mut bufb);
                        },
                        _ => {
                            client.close().unwrap();
                            panic!("Broken peer");
                        }
                    }
                    println!("Valid client");
                    client.close().unwrap();
                    punchnat(&sock,antype,magic);
                    tunnel_server(&sock,raddrs.as_slice());
                } else {
                    client.close().unwrap();
                }
            });
        }
    }
}

